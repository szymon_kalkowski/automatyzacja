package com.example.demo.student;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("!test")
@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository) {
        return args -> {
            Student szymon = new Student("Szymon Kalkowski", "szymon.kalkowski@wp.pl",
                    LocalDate.of(2002, Month.FEBRUARY, 6));
            Student jan = new Student("Jan Kowalski", "jan.kowalski@gmail.com", LocalDate.of(2000, Month.JANUARY, 1));

            repository.saveAll(List.of(szymon, jan));
        };
    }
}
