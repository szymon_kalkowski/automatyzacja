package com.example.demo.student;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class StudentServiceTest {

    private StudentService studentService;
    @Mock
    private StudentRepository studentRepository;

    @BeforeEach
    void setUp() {
        studentService = new StudentService(studentRepository);
    }

    @Test
    void testGetAllStudents() {
        // given
        Mockito.when(studentRepository.findAll()).thenReturn(List.of(new Student(), new Student()));
        // when
        List<Student> students = studentService.getStudents();
        // then
        assertEquals(2, students.size());
    }

    @Test
    void testAddNewStudent() {
        // given
        Mockito.when(studentRepository.findStudentByEmail(Mockito.anyString())).thenReturn(Optional.empty());
        Student student = new Student();
        student.setEmail("szymon.kalkowski@wp.pl");
        // when
        studentService.addNewStudent(student);
        // then
        Mockito.verify(studentRepository).save(student);
    }

    @Test
    void testAddNewStudentWithTakenEmail() {
        // given
        Mockito.when(studentRepository.findStudentByEmail(Mockito.anyString())).thenReturn(Optional.of(new Student()));
        Student student = new Student();
        student.setEmail("szymon.kalkowski@wp.pl");
        // when
        Exception exception = assertThrows(IllegalStateException.class, () -> {
            studentService.addNewStudent(student);
        });
        // then
        assertEquals("Email taken", exception.getMessage());
    }

    @Test
    void testDeleteStudent() {
        // given
        Mockito.when(studentRepository.existsById(Mockito.anyLong())).thenReturn(true);
        // when
        studentService.deleteStudent(1L);
        // then
        Mockito.verify(studentRepository).deleteById(1L);
    }

    @Test
    void testDeleteStudentWhoDoesntExists() {
        // given
        Mockito.when(studentRepository.existsById(Mockito.anyLong())).thenReturn(false);
        // when
        Exception exception = assertThrows(IllegalStateException.class, () -> {
            studentService.deleteStudent(1L);
        });
        // then
        assertEquals("Student with id 1 does not exist", exception.getMessage());
    }

    @Test
    void testUpdateStudentWhoDoesntExists() {
        // given
        Mockito.when(studentRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
        // when
        Exception exception = assertThrows(IllegalStateException.class, () -> {
            studentService.updateStudent(1L, null, null);
        });
        // then
        assertEquals("Student with id 1 does not exist", exception.getMessage());
    }

    @Test
    void testUpdateStudent() {
        // given
        Student student = new Student();
        student.setName("Name1");
        student.setEmail("Email1");
        Mockito.when(studentRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(student));
        Mockito.when(studentRepository.findStudentByEmail(Mockito.anyString())).thenReturn(Optional.empty());
        // when
        studentService.updateStudent(1L, "Name2", "Email2");
        // then
        assertEquals("Name2", student.getName());
        assertEquals("Email2", student.getEmail());
    }
}
