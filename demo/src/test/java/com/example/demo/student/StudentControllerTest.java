package com.example.demo.student;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest
public class StudentControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StudentService studentService;

    @Test
    void testGetStudents() throws Exception {
        // given
        Mockito.when(studentService.getStudents())
                .thenReturn(List.of(new Student("Szymon", "s@wp.pl", LocalDate.now())));
        // when
        ResultActions result = mockMvc.perform(get("/api/v1/student"));
        // then
        result.andExpect(status().isOk());
        result.andExpect(jsonPath("$").isArray());
        result.andExpect(jsonPath("$[0]").exists());
    }

    @Test
    void testAddNewStudent() throws Exception {
        // when
        ResultActions result = mockMvc
                .perform(post("/api/v1/student")
                        .contentType("application/json")
                        .content("{\"name\":\"Szymon\",\"email\":\"x@gmail.com\",\"dob\":\"2021-06-06\"}"));
        // then
        result.andExpect(status().isOk());
        Mockito.verify(studentService).addNewStudent(Mockito.any());
    }

    @Test
    void testDeleteStudent() throws Exception {
        // when
        ResultActions result = mockMvc.perform(delete("/api/v1/student/1"));
        // then
        result.andExpect(status().isOk());
        Mockito.verify(studentService).deleteStudent(1L);
    }

    @Test
    void testUpdateStudent() throws Exception {
        // when
        ResultActions result = mockMvc
                .perform(put("/api/v1/student/1")
                        .param("name", "Szymon")
                        .param("email", "x@gmail.com"));
        // then
        result.andExpect(status().isOk());
        Mockito.verify(studentService).updateStudent(1L, "Szymon", "x@gmail.com");
    }

}
