package com.example.demo.student;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StudentControllerIntegrationTest {
    @LocalServerPort
    private int port;
    private TestRestTemplate restTemplate = new TestRestTemplate();
    @Autowired
    private StudentRepository studentRepository;

    @BeforeEach
    void setUp() {
        studentRepository.saveAll(List.of(new Student("Szymon Kalkowski", "szymon.kalkowski@wp.pl",
                LocalDate.of(2002, Month.FEBRUARY, 6)),
                new Student("Jan Kowalski", "jan.kowalski@gmail.com", LocalDate.of(2000, Month.JANUARY, 1))));
    }

    @AfterEach
    void tearDown() {
        studentRepository.deleteAll();
    }

    @Test
    void testGetAllStudents() {
        // given
        var url = "http://localhost:" + port + "/api/v1/student";
        // when
        var response = restTemplate.getForEntity(url, List.class);
        // then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(2, response.getBody().size());
    }

    @Test
    void testAddNewStudent() {
        // given
        var url = "http://localhost:" + port + "/api/v1/student";
        Student student = new Student("Szymon", "e@gmail.com", LocalDate.now());
        HttpEntity<Student> request = new HttpEntity<>(student);
        // when
        var response = restTemplate.postForEntity(url, request, Void.class);
        // then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(3, studentRepository.findAll().size());
    }

    @Test
    void testDeleteStudent() {
        // given
        var student = studentRepository.findAll().get(0);
        var url = "http://localhost:" + port + "/api/v1/student/" + student.getId();
        // when
        restTemplate.delete(url);
        // then
        assertEquals(1, studentRepository.findAll().size());
    }

    @Test
    void testUpdateStudent() {
        // given
        var url = "http://localhost:" + port + "/api/v1/student/1?name=Wojtek&email=xxx@gmail.com";
        // when
        restTemplate.put(url, null);
        // then
        var student = studentRepository.findById(1L).get();
        assertEquals("Wojtek", student.getName());
        assertEquals("xxx@gmail.com", student.getEmail());
    }

}
