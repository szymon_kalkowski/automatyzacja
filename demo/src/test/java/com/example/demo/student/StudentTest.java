package com.example.demo.student;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

public class StudentTest {
    @Test
    void testGetStudentAge() {
        // given
        Student student = new Student();
        student.setDob(LocalDate.now().minusYears(21));
        // when
        int age = student.getAge();
        // then
        assertEquals(21, age);
    }
}
